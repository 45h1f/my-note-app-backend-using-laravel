<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Note;
use Illuminate\Http\Request;

class NoteController extends Controller
{

    public function index(Request $request)
    {
        $id = $request->get("id");
        return Note::where('user_id', $id)->orderBy('id', 'desc')->get();
    }

    public function store(Request $request)
    {

//        $request->validate([
//            'user_id' => 'required',
//            'title' => 'required',
//            'note' => 'required',
//        ]);
//        return $request;

        $result = Note::create($request->all());
        return $result;
    }


    public function show(Note $note)
    {
        return $note;
    }


    public function update(Request $request, Note $note)
    {
        $request->validate([
            'user_id' => 'required',
            'title' => 'required',
            'note' => 'required',
        ]);
        $note->update($request->all());

        return $note;
    }


    public function destroy(Note $note)
    {
        $note->delete();
        return true;
    }

    public function delete(Request $request)
    {
        return $request;
//        $note->delete();
        return true;
    }
}
