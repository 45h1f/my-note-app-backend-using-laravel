<?php

Auth::routes();


Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('notes', 'NoteController');
    Route::resource('users', 'UserController');

});


