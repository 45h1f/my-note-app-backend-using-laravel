@extends('layouts.app')

@section('content')
    @include('includes.header')

    <br><br><br>

    <div class="container">
        <div class="row w-100">
            <div class="col-md-6">
                <div class="card border-info mx-sm-1 p-3">
                    <div class="card border-info shadow text-info p-3 my-card" ><span class="fa fa-car" aria-hidden="true"></span></div>
                    <div class="text-info text-center mt-3"><h4>Users</h4></div>
                    <div class="text-info text-center mt-2"><h1>{{$total_users}}</h1></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card border-success mx-sm-1 p-3">
                    <div class="card border-success shadow text-success p-3 my-card"><span class="fa fa-eye" aria-hidden="true"></span></div>
                    <div class="text-success text-center mt-3"><h4>Notes</h4></div>
                    <div class="text-success text-center mt-2"><h1>{{$total_notes}}</h1></div>
                </div>
            </div>

        </div>
    </div>
@endsection
